﻿using PD32Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop
{
    public class ProductManager
    {
		DBItem<Product> dbProduct;

		public ProductManager(DBItem<Product> dbProduct)
		{
			this.dbProduct = dbProduct;
		}

		public void ProductCreate()
		{
			Console.Write("ProductCreate: Name: ");
			string name = Console.ReadLine();
			int categoryId = Helper.CheckIntImput("ProductCreate: CategoryId:");
			int price = Helper.CheckIntImput("ProductCreate: Price:");

			Product product = new Product(name, categoryId, price);
			dbProduct.AddItem(product);
		}

		public void ShowProduct()
		{
			foreach (Product product in dbProduct.Items)
			{
				Console.WriteLine(product);
			}
		}
	}
}
