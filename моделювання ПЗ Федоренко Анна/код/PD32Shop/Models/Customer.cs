﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class Customer : Person, IEntity
    {

        private static int count = 1;
        public int Id { get; set; }
        public int Age { get; set; }
        public Customer(string FirstName, string LastName, int Age) : base(FirstName, LastName)
        {
            this.Id = count++;
            this.Age = Age;
        }
        public override string ToString()
        {
            return String.Format(Id + " " + base.ToString() + " " + Age);
        }
    }
}
