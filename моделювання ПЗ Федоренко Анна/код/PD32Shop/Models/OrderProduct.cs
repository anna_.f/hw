﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class OrderProduct : IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public OrderProduct(int OrderId, int ProductId, int Quantity)
        {
            this.Id = count++;
            this.OrderId = OrderId;
            this.ProductId = ProductId;
            this.Quantity = Quantity;
        }

        public override string ToString()
        {
            return String.Format(Id + " " + OrderId + " " + ProductId + " " + Quantity);
        }
    }
}
