﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class Product : IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int Price { get; set; }

        public Product(string Name, int CategoryId, int Price)
        {
            this.Id = count++;
            this.Name = Name;
            this.CategoryId = CategoryId;
            this.Price = Price;
        }
        public override string ToString()
        {
            return String.Format(Id + " " + Name + " " + CategoryId + " " + Price);
        }
    }
}
