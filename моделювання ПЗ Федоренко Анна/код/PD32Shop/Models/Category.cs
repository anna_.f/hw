﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class Category : IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public string Name { get; set; }

        public Category(string Name)
        {
            this.Id = count++;
            this.Name = Name;
        }
        public override string ToString()
        {
            return String.Format(Id + " " + Name);
        }
    }
}
