﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class Description : IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public string Info { get; set; }
        public int ProductId { get; set; }
        public Description(string Info, int ProductId)
        {
            this.Id = count++;
            this.Info = Info;
            this.ProductId = ProductId;
        }
        public override string ToString()
        {
            return String.Format(Id + " " + Info + " " + ProductId);
        }
    }
}
