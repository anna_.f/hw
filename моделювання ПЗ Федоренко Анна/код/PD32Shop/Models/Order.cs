﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
   public class Order : IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public int EmployeeId { get; set; }

        public Order(int CustomerId, string Name, int EmployeeId)
        {
            this.Id = count++;
            this.CustomerId = CustomerId;
            this.Name = Name;
            this.EmployeeId = EmployeeId;
        }
        public override string ToString()
        {
            return String.Format(Id + " " + CustomerId + " " + Name + " " + EmployeeId);
        }
    }
}
