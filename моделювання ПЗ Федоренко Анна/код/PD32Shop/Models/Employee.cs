﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop.Models
{
    public class Employee : Person, IEntity
    {
        private static int count = 1;
        public int Id { get; set; }
        public string Position { get; set; }
        public int ChiefId { get; set; }
        public decimal Salary { get; set; }
        public Employee(string FirstName, string LastName, string Position, int ChiefId, decimal Salary) : base(FirstName, LastName)
        {
            this.Id = count++;
            this.Position = Position;
            this.ChiefId = ChiefId;
            this.Salary = Salary;
        }

        public override string ToString()
        {
            return String.Format(Id + " " + base.ToString() + " " + Position + " " + ChiefId + " " + Salary);
        }
    }
}
