﻿using PD32Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop
{
    public class DBItem<T> where T : IEntity
    {
        public List<T> Items { get; set; }
        public DBItem()
        {
            Items = new List<T>();
        }
        public void AddItem(T item)
        {
            Items.Add(item);
        }
        public bool DeleteItem(T item)
        {
            return Items.Remove(item);
        }
        public T FindByID(int id)
        {
            foreach (T item in Items)
            {
                if (item.Id == id)
                    return item;

            }
            return default(T);
        }
    }
}
