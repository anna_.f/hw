﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop
{
    static class Helper
    {
		public static int CheckIntImput(string message)
		{
			int result;
			bool isDone = false;
			do
			{
				Console.Write(message);
				string str = Console.ReadLine();
				isDone = int.TryParse(str, out result);
				if (isDone == false)
					Console.WriteLine("Incorrect data : " + str);
			} while (!isDone);
			return result;
		}
	}
}
