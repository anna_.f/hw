﻿using PD32Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD32Shop
{
    class Program
    {
		static void Main(string[] args)
		{
			DBItem<Customer> dbCustomer = new DBItem<Customer>();
			Customer customer1 = new Customer("Oleksii", "Oleksienko", 20);
			Customer customer2 = new Customer("Vitaliy", "Vasylenko", 32);
			Customer customer3 = new Customer("Dmytro", "Dmytrenko", 18);
			dbCustomer.AddItem(customer1);
			dbCustomer.AddItem(customer2);
			dbCustomer.AddItem(customer3);

			DBItem<Employee> dbEmployee = new DBItem<Employee>();
			Employee employee1 = new Employee("Ivan", "Ivanenko", "Chief", 0, 1400);
			Employee employee2 = new Employee("Petro", "Petrenko", "Sales manager", 1, 800);
			Employee employee3 = new Employee("Sydor", "Sydorenko", "Store manager", 1, 500);

			dbEmployee.AddItem(employee1);
			dbEmployee.AddItem(employee2);
			dbEmployee.AddItem(employee3);

			DBItem<Category> dbCategory = new DBItem<Category>();
			Category category1 = new Category("Phone");
			Category category2 = new Category("Laptop");
			dbCategory.AddItem(category1);
			dbCategory.AddItem(category2);


			DBItem<Product> dbProduct = new DBItem<Product>();
			Product product1 = new Product("Iphone", category1.Id, 1000);
			Product product2 = new Product("MacBook 1", category2.Id, 2400);
			Product product3 = new Product("Dell 1", category2.Id, 1500);
			dbProduct.AddItem(product1);
			dbProduct.AddItem(product2);
			dbProduct.AddItem(product3);


			DBItem<Description> dbDescription = new DBItem<Description>();
			Description description1 = new Description("Apple phone", product1.Id);
			Description description2 = new Description("Dell laptop", product3.Id);
			Description description3 = new Description("Apple laptop", product2.Id);
			dbDescription.AddItem(description1);
			dbDescription.AddItem(description2);
			dbDescription.AddItem(description3);


			DBItem<Order> dbOrder = new DBItem<Order>();
			Order order1 = new Order(customer1.Id, "Order1", employee1.Id);
			Order order2 = new Order(customer2.Id, "Order2", employee2.Id);
			dbOrder.AddItem(order1);
			dbOrder.AddItem(order2);


			DBItem<OrderProduct> dbOrderProduct = new DBItem<OrderProduct>();
			OrderProduct orderProd1 = new OrderProduct(order1.Id, product2.Id, 2);
			OrderProduct orderProd2 = new OrderProduct(order2.Id, product3.Id, 2);
			OrderProduct orderProd3 = new OrderProduct(order2.Id, product1.Id, 1);
			dbOrderProduct.AddItem(orderProd1);
			dbOrderProduct.AddItem(orderProd2);
			dbOrderProduct.AddItem(orderProd3);

			ProductManager productManager = new ProductManager(dbProduct);
			productManager.ShowProduct();
			productManager.ProductCreate();
			productManager.ShowProduct();
			Console.ReadLine();
			foreach (Customer customer in dbCustomer.Items)
			{
				Console.WriteLine(customer);
				foreach (Order order in dbOrder.Items)
				{
					if (order.CustomerId == customer.Id)
					{
						Console.WriteLine("\t" + order);
						foreach (Employee employee in dbEmployee.Items)
						{
							if (order.EmployeeId == employee.Id)
							{
								Console.WriteLine("\t\t" + employee);
							}
						}
						foreach (OrderProduct oerderProd in dbOrderProduct.Items)
						{
							if (oerderProd.OrderId == order.Id)
							{
								foreach (Product prod in dbProduct.Items)
								{
									if (oerderProd.ProductId == prod.Id)
									{
										Console.WriteLine("\t\t" + prod);
										foreach (Category category in dbCategory.Items)
										{
											if (category.Id == prod.CategoryId)
											{
												Console.WriteLine("\t\t\t" + category);
											}
										}
										foreach (Description description in dbDescription.Items)
										{
											if (description.ProductId == prod.Id)
											{
												Console.WriteLine("\t\t\t" + description);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
    }
}
